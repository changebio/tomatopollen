Comparing tomato and Arabidopsis pollen gene expression
=======================================================

Previous RNA-Seq analysis identified genes that were abundantly expressed in Arabidopsis pollen.

This Markdown aims to compare and contast the genes expressed in tomato pollen with genes expressed in Arabidopsis.

Questions we aimed to answer include:

* How similar are the gene expression profiles between the two species?
* The Arabidopsis pollen RNA-Seq data set reported high expression for RALF proteins and pectate lyase proteins. Are these same classes of genes also highly expressed in tomoto pollen?

Analysis
--------

Create a data set containing normalized counts per million for tomato genes:

```{r}
library(edgeR)
d=read.delim('../Counts/results/tomato_counts.tsv')
group=c('C','C','C','C','C','T','T','T','T','T')
dge=DGEList(d,group=group)
dge=calcNormFactors(dge)
cpms=cpm(dge)
cn=grep('C',colnames(cpms))
tr=grep('T',colnames(cpms))
ave.cn=apply(cpms[,cn],1,mean)
ave.tr=apply(cpms[,tr],1,mean)
d=data.frame(locus=row.names(d),ave.cn,ave.tr)
sizes=read.delim('../ExternalDataSets/tx_sizes.txt.gz',as.is=T)
d=merge(d,sizes)
annots_file='../ExternalDataSets/S_lycopersicum_Feb_2014.bed'
annots=read.delim(annots_file,sep='\t',header=F)[,c(4,13,14)]
names(annots)=c('transcript','locus','description')
d=merge(d,annots[,c('locus','description')])
d$kb=d$bp/1000
fkm=data.frame(locus=d$locus,ave.cn=d$ave.cn/d$kb,
               ave.tr=d$ave.tr/d$kb,
               description=d$description)
o=order(fkm$ave.cn,decreasing=T)
fkm=fkm[o,]
```

Identify Arabidopsis homologs
-----------------------------

I used the BLASTP program (version 2.2.29) to search the tomato ITAG2.4 proteins against TAIR10 proteins downloaded from TAIR as file TAIR10_pep_20101214. The search was run using default parameters except for -evalue which was 0.01, -max_target_seqs which was 1, -num_threads which was 6, and -outfmt "7 qseqid qlen qframe qstart qe
nd slen sstart send qcovs qcovhsp pident evalue bitscore ssid stitle", the same options I've used to run similar searches using BLASTX. 

Results are in ../ExternalDataSets/blastp_tair10.tsv.gz. The query and subject ids contained both the gene models ids and some additional text, so I removed the extra text using a script called tidyBlastpResults.py. The output from that is blastp_tair10_tidy.tsv.gz, which I'll use in the next steps.

```{r}
f='../ExternalDataSets/blastp_tair10_tidy.tsv.gz'
sol2at=read.delim(f,sep='\t',as.is=T)
# add tomato locus ids
sol2at=merge(sol2at,annots,by.x='q.id',by.y='transcript')
# add AGI locus codes
agi=sapply(strsplit(sol2at$s.title,'\\.'),function(x)x[[1]])
sol2at$agi=agi
v=!is.na(sol2at$s.title)
sol2at=sol2at[v,]
tokeep=sol2at$percent.identity>=30&sol2at$evalue<=0.0001
sol2at=sol2at[tokeep,c('locus','agi')]
sol2at=sol2at[!duplicated(sol2at),]
```

Merge with tomato rpkm, keeping all tomato rows to ensure we don't lose any genes because we couldn't find an Arabidopsis homolog:

```{r}
fkm=merge(fkm,sol2at,all.x=T)
```

Retrieve Arabidopsis pollen RPKM file:

```{r}
u='http://www.igbquickload.org/pollen/SupplementalDataFiles/Revised/new_pollen_vs_seedling_RPKM.tsv'
# requires internet connection, may be a little slow
at=read.delim(u,sep='\t',as.is=T,header=T,comment.char='#') 
at=at[,c('AGI','pollen','Ave.seedling')]
```

Merge with tomato fkm and re-order

```{r}
fkm=merge(fkm,at,by.x='agi',by.y='AGI',all.x=T)
o=order(fkm$ave.cn,decreasing=T)
fkm=fkm[o,c('locus','agi','ave.cn','ave.tr','pollen','Ave.seedling','description')]
```

#### Correlation between pollen samples

We can compare expression values between pollen samples using Spearman's rank correlation coefficient. It will allow us to get assess how similar the ordering of genes is between samples. In other words, we can ask the question: On average, are the genes that are high in tomato also high in Arabidopsis?

Calculate the rank correlation coefficient:

```{r}
pollen.cn=cor(x=fkm$ave.cn,y=fkm$pollen,method='spearman',use="complete.obs")
seedling.cn=cor(x=fkm$ave.cn,y=fkm$Ave.seedling,method='spearman',use="complete.obs")
pollen.tr=cor(x=fkm$ave.tr,y=fkm$pollen,method='spearman',use="complete.obs")
seedling.tr=cor(x=fkm$ave.tr,y=fkm$Ave.seedling,method='spearman',
                use='complete.obs')
```

Correlation between control and treatment tomato pollen and Arabidopsis pollen was `r pollen.cn` and `r pollen.tr`. 

Correlation between control and treatment tomato pollen and Arabidopsis rosettes was `r seedling.cn` and `r seedling.tr`.

The overall expression level of pollen genes in Arabidopsis and tomato is similar in the sense that the rankings appear to be preserved. However, it's important to note that this is a very crude analysis. Our method of matching genes between species is based solely on sequence similarity and blast hits, which may break down for large gene families, such as pectate lyases and RALFs.

The ranking of genes is similar between Arabidopsis and tomato. Let's look at this another way, using hierarchical clustering. In these scheme, we'll ask whether the distance between samples is related to sample type or not.

#### Heirarchical clustering of pollen samples from tomato and Arabidopsis

```{r}
# remove rows with no homologies
v=!is.na(fkm$agi)
d=fkm[v,]
d.t=t(d)[c('ave.cn','ave.tr','pollen','Ave.seedling'),]
row.names(d.t)=c('Le-Pollen-Cn',
                 'Le-Pollen-Tr','At-Pollen','At-Rosettes')
distance=dist(d.t,method="manhattan")
# options "euclidean", "maximum", "manhattan", "canberra", "binary" or "minkowski"
# euclidean is default
clusters=hclust(distance) # does hierarchical clustering
plot(clusters)
```

These results were hard to interpret because using different distance metrics changed the results, suggesting the methods may be picking up on different trends in the data. For example, arabidopsis samples might cluster because they are from the same species, while pollen samples might cluster because they are from the same sample type. These results are inconclusive.

Different gene to gene mappings
-------------------------------

Gad Miller provided a spreadsheet that maps tomato ids onto Arabidospis ids. He made it using the BioMart tool at EBI. Let's repeat the process using this alterantive mapping. 

```{r}
fname='../ExternalDataSets/agi.csv'
colClasses=rep('character',3)
a2s=read.csv(fname,colClasses=colClasses,na.strings='#N/A')[,c('locus','agi')]
fkm2=fkm[,c('locus','ave.cn','ave.tr','description')]
fkm2=merge(fkm2,a2s,by.x='locus',by.y='locus',all.x=T)
fkm2=merge(fkm2,at,by.x='agi',by.y='AGI',all.x=T)
o=order(fkm2$ave.cn,decreasing=T)
fkm2=fkm2[o,c('locus','agi','ave.cn','ave.tr','pollen','Ave.seedling','description')]
```

Calculate the rank correlation coefficient:

```{r}
pollen.cn2=cor(x=fkm2$ave.cn,y=fkm2$pollen,method='spearman',use="complete.obs")
seedling.cn2=cor(x=fkm2$ave.cn,y=fkm2$Ave.seedling,method='spearman',use="complete.obs")
pollen.tr2=cor(x=fkm2$ave.tr,y=fkm2$pollen,method='spearman',use="complete.obs")
seedling.tr2=cor(x=fkm2$ave.tr,y=fkm2$Ave.seedling,method='spearman',
                use='complete.obs')
```

Correlation between control and treatment tomato pollen and Arabidopsis pollen was `r pollen.cn2` and `r pollen.tr2`. 

Correlation between control and treatment tomato pollen and Arabidopsis rosettes was `r seedling.cn2` and `r seedling.tr2`.

Correlations between pollen increased slightly and the correlations between pollen and seedling went down a little. 

Maybe the ensembl mappings are better? 

Comparing the mappings:

```{r}
same=fkm$agi==fkm2$agi
total1=sum(!is.na(fkm$agi))
total2=sum(!is.na(fkm2$agi))
```

The blastp mapping reported Arabidopsis homologs for `r sum(total1)` tomato genes. The ensembl mapping from Gad reported Arabidopsis homologs for `r sum(total2)` tomato genes. The two methods reported the same Arabidopsis gene for `r sum(same,na.rm=T)` tomato genes.

Calculate the rank correlation coefficient using the subset of genes where both methods agreed on the identity of the Arabidopsis homolog:


```{r}
d=fkm[same,]
d=d[!is.na(d$agi),]
pollen.cn3=cor(x=d$ave.cn,y=d$pollen,method='spearman',use="complete.obs")
seedling.cn3=cor(x=d$ave.cn,y=d$Ave.seedling,method='spearman',use="complete.obs")
pollen.tr3=cor(x=d$ave.tr,y=d$pollen,method='spearman',use="complete.obs")
seedling.tr3=cor(x=d$ave.tr,y=d$Ave.seedling,method='spearman',
                use='complete.obs')
```

Correlation between control and treatment tomato pollen and Arabidopsis pollen was `r pollen.cn2` and `r pollen.tr2`. 

Correlation between control and treatment tomato pollen and Arabidopsis rosettes was `r seedling.cn2` and `r seedling.tr2`.

Correlations between pollen decreased a little and correlations between pollen and seedling went down a lot.

Visualize the data using a scatterplot:

```{r}
logs=data.frame(ave.cn=log10(d$ave.cn),
                ave.tr=log10(d$ave.tr),
                At.pollen=log10(d$pollen),
                At.rosette=log10(d$Ave.seedling))
plot(logs,pch='.',main="Comparing log10 expression")
```



Conclusion
----------

Gene expression profiles between tomato and Arabidopsis pollen samples were more similar than profiles between tomato pollen and Arabidopsis rosettes. However, the comparison was complicated by the difficulty of identifying homologous genes between species. Using the ensembl mapping, we achieved slightly better overall correlation. Using just the genes that mapped to the same Arabidopsis homolog in both methods yielded about the same correlation for pollen samples but significantly worse correlation between rosette and pollen samples. 

Overall, the gene expression level correlation and rank between pollen samples from different species was 
